class ContactScreen < PM::Screen
  include SPORTS::StyleModule
  stylesheet :sports

  title "Contact"

  include MapKit

  def view_did_load
    # Load from Core Data
    layout(self.view, :root) do
      @map_view = subview(MapView, :contact_map_view)
      @table_view = subview(UITableView, :contact_table_view)
    end

    @map_view.delegate = self
    @map_view.zoom_enabled = true
    @map_view.shows_user_location = true

    @table_view.dataSource = self
    @table_view.delegate = self
  end

  def on_present
    if Device.ios_version.to_i >= 7
      self.navigationController.navigationBar.barTintColor = '#3D3D3D'.to_color
      self.navigationController.navigationBar.tintColor = '#FFF'.to_color
      self.navigationController.navigationBar.setTitleTextAttributes(NSForegroundColorAttributeName => '#fff'.to_color)
    end
  end

  def on_load
    set_attributes self.view,
      stylename: :app
    menu = UIBarButtonItem.imaged("menu") do
      self.slidingPanelController.openLeftPanel
    end
    self.navigationItem.leftBarButtonItem = menu
  end

# Table View Delegate
  def numberOfSectionsInTableView(table_view)
    1
  end

  # Number of cells
  def tableView(table_view, numberOfRowsInSection:section)
    1
  end

  def tableView(table_view, cellForRowAtIndexPath:index_path)
    @reuseIdentifier ||= "CONTACT_CELL"
    cell = @table_view.dequeueReusableCellWithIdentifier(@reuseIdentifier) || begin
      UITableViewCell.alloc.initWithStyle(UITableViewCellStyleSubtitle, reuseIdentifier:@reuseIdentifier)
    end

    cell.textLabel.text = "Helpline"
    cell.detailTextLabel.text = "90228540"

    cell
  end

  def tableView(tableView, didSelectRowAtIndexPath: index_path)
    # Call
    "tel://90228540".nsurl.open
  end

end