class ScoreScreen < PM::WebScreen

  title "Score Updates"

  def content
    self.webview.scalesPageToFit = true
    "http://www.qrcoderace.com/scores".nsurl
  end

  def on_present
    if Device.ios_version.to_i >= 7
      self.navigationController.navigationBar.barTintColor = '#3D3D3D'.to_color
      self.navigationController.navigationBar.tintColor = '#FFF'.to_color
      self.navigationController.navigationBar.setTitleTextAttributes(NSForegroundColorAttributeName => '#fff'.to_color)
    end
  end

  def on_load
    set_attributes self.view,
      stylename: :app
    menu = UIBarButtonItem.imaged("menu") do
      self.slidingPanelController.openLeftPanel
    end
    self.navigationItem.leftBarButtonItem = menu
  end
end