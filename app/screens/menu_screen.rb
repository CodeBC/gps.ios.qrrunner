class MenuScreen < PM::TableScreen
  stylesheet :screen

  def table_view
    super.tap do |t|
      t.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleWidth
      t.backgroundColor = UIColor.clearColor
      t.backgroundView = nil
      t.separatorStyle = UITableViewCellSeparatorStyleNone
      t.bounces = false
      t
    end
  end

  def table_height
    60
  end

  def table_data
    [{
      cells: [
        { title: "", height: table_height, background_color: UIColor.clearColor },
        { title: "", height: table_height, background_color: UIColor.clearColor },
        { title: "QR Code Reader", height: table_height, background_color: UIColor.clearColor, action: :menu_tapped, arguments: {item: "qr"}},
        { title: "Scores Updates", height: table_height, background_color: UIColor.clearColor, action: :menu_tapped, arguments: {item: "scores"}},
        { title: "Map", height: table_height, background_color: UIColor.clearColor, action: :menu_tapped, arguments: {item: "map"}},
        { title: "Helpline", height: table_height, background_color: UIColor.clearColor, action: :menu_tapped, arguments: {item: "helpline"}},
      ]
    }]
  end

  def preferredStatusBarStyle
    UIStatusBarStyleLightContent
  end

  def tableView(table_view, willDisplayCell: table_cell, forRowAtIndexPath: index_path)
    super
    table_cell.textLabel.color = UIColor.whiteColor
  end

  def create_table_cell(data_cell)
    super.tap do |cell|
      cell.selectionStyle = UITableViewCellSelectionStyleNone
      cell
    end
  end

  def menu_tapped(args)
    if args[:item] == 'qr'
      self.slidingPanelController.setCenterViewController UINavigationController.alloc.initWithRootViewController(MainScreen.new({nav_bar: true}))
    elsif args[:item] == 'scores'
      self.slidingPanelController.setCenterViewController UINavigationController.alloc.initWithRootViewController(ScoreScreen.new({nav_bar: true}))
    elsif args[:item] == 'map'
      self.slidingPanelController.setCenterViewController UINavigationController.alloc.initWithRootViewController(MapScreen.new({nav_bar: true}))
    else
      self.slidingPanelController.setCenterViewController UINavigationController.alloc.initWithRootViewController(ContactScreen.new({nav_bar: true}))
    end

    self.slidingPanelController.closePanel
  end
end