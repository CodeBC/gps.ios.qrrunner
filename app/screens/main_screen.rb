class MainScreen < PM::Screen
  include SPORTS::StyleModule
  stylesheet :sports

  title "QR Code Reader"

  def view_did_load
    layout(self.view, :root) do
      @scan_view = subview(UIView, :scan_view)
    end


    self.view.bringSubviewToFront(@scan_view)
  end

  def viewWillAppear(animated)
    super(animated)
    setupCapture
  end

  def stopRunning
    @session.stopRunning
  end

  def setupCapture
    @session = AVCaptureSession.alloc.init
    @session.sessionPreset = AVCaptureSessionPresetHigh

    @device = AVCaptureDevice.defaultDeviceWithMediaType AVMediaTypeVideo
    @error = Pointer.new('@')
    @input = AVCaptureDeviceInput.deviceInputWithDevice @device, error: @error

    @previewLayer = AVCaptureVideoPreviewLayer.alloc.initWithSession(@session)
    @previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill
    layerRect = self.view.layer.bounds
    @previewLayer.bounds = layerRect
    @previewLayer.setPosition(CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect)))
    self.view.layer.addSublayer(@previewLayer)

    @queue = Dispatch::Queue.new('camQueue')
    @output = AVCaptureMetadataOutput.alloc.init
    @output.setMetadataObjectsDelegate self, queue: @queue.dispatch_object

    @session.addInput @input
    @session.addOutput @output
    @output.metadataObjectTypes = [ AVMetadataObjectTypeQRCode ]

    @session.startRunning
    NSLog "session running: #{@session.running?}"
    true
  end

  def captureOutput(captureOutput, didOutputMetadataObjects: metadataObjects, fromConnection: connection)
    Dispatch::Queue.main.async do
      open_modal WebScreen.new url: metadataObjects[0].stringValue, nav_bar: true
      stopRunning
      true
    end
  end

  def on_present
    if Device.ios_version.to_i >= 7
      self.navigationController.navigationBar.barTintColor = '#3D3D3D'.to_color
      self.navigationController.navigationBar.tintColor = '#FFF'.to_color
      self.navigationController.navigationBar.setTitleTextAttributes(NSForegroundColorAttributeName => '#fff'.to_color)
    end
  end

  def on_load
    set_attributes self.view,
      stylename: :app
    menu = UIBarButtonItem.imaged("menu") do
      self.slidingPanelController.openLeftPanel
    end
    self.navigationItem.leftBarButtonItem = menu
  end
end