Teacup::Stylesheet.new :sports do
  style :app, backgroundColor: '#fff'.to_color

  style :contact_map_view,
    frame: [[0, 0], ['100%', '40%']],
    backgroundColor: '#e5e5e5'.to_color,
    autoresizingMask: autoresize.flexible_width | autoresize.flexible_height

  style :contact_table_view,
    frame: [[0, '40%'], ['100%', '60%']],
    backgroundColor: '#fff'.to_color,
    autoresizingMask: autoresize.flexible_width | autoresize.flexible_height

  style :scan_view,
    frame: [[0, 60], ['100%', '100%']],
    autoresizingMask: autoresize.flexible_width | autoresize.flexible_height
end