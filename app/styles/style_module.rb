module SPORTS
  module StyleModule
    def setup_navbar
      if Device.ios_version.to_i >= 7
        self.navigationController.navigationBar.barTintColor = '#3598DB'.to_color
        self.navigationController.navigationBar.tintColor = '#FFF'.to_color
      end
    end
  end
end