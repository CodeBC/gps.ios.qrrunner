class AppDelegate < PM::Delegate
  def on_load(app, options)

    menu = MenuScreen.new

    main = UINavigationController.alloc.initWithRootViewController MainScreen.new(nav_bar: true)

    slidingPanelController = MSSlidingPanelController.alloc.initWithCenterViewController(main, leftPanelController:menu, andRightPanelController:nil)
    open_root_screen slidingPanelController

    self.window.setBackgroundColor UIColor.colorWithPatternImage(UIImage.imageNamed("background.png"))
  end

  def touchesBegan(touches, withEvent: event)
    super.touchesBegan(touches, withEvent: event)
    location = event.allTouches.anyObject.locationInView self.window
    if location.y > 0 && location.y < 20
      App.notification_center.post 'touchStatusBarClick'
    end
  end

end
